/* Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#include <linux/mm.h>
#include <linux/init.h>

#include <asm/mach/map.h>

#include <mach/hardware.h>

/* Page table mapping for I/O region */
static struct map_desc moxart_io_desc[] __initdata = {
	{
		.virtual	= IO_ADDRESS(MOXART_APB_DMA_BASE),
		.pfn		= __phys_to_pfn(MOXART_APB_DMA_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {
		.virtual	= IO_ADDRESS(MOXART_MMC_BASE),
		.pfn		= __phys_to_pfn(MOXART_MMC_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {
		.virtual	= IO_ADDRESS(MOXART_TIMER_BASE),
		.pfn		= __phys_to_pfn(MOXART_TIMER_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {
		/*apparently, RTC is handled using PMU base */
		/*.virtual	= IO_ADDRESS(MOXART_RTC_BASE),
		.pfn		= __phys_to_pfn(MOXART_RTC_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {*/
		.virtual	= IO_ADDRESS(MOXART_PMU_BASE),
		.pfn		= __phys_to_pfn(MOXART_PMU_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {
		.virtual	= IO_ADDRESS(MOXART_GPIO_BASE),
		.pfn		= __phys_to_pfn(MOXART_GPIO_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {
		.virtual    = IO_ADDRESS(MOXART_UART1_BASE),
		.pfn		= __phys_to_pfn(MOXART_UART1_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {
		.virtual	= IO_ADDRESS(MOXART_UART2_BASE),
		.pfn		= __phys_to_pfn(MOXART_UART2_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {
		.virtual	= IO_ADDRESS(MOXART_INTERRUPT_BASE),
		.pfn		= __phys_to_pfn(MOXART_INTERRUPT_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {
		.virtual    = IO_ADDRESS(MOXART_FTMAC1_BASE),
		.pfn		= __phys_to_pfn(MOXART_FTMAC1_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {
		.virtual	= IO_ADDRESS(MOXART_FTMAC2_BASE),
		.pfn		= __phys_to_pfn(MOXART_FTMAC2_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {
		.virtual	= IO_ADDRESS(MOXART_WATCHDOG_BASE),
		.pfn		= __phys_to_pfn(MOXART_WATCHDOG_BASE),
		.length		= SZ_4K,
		.type		= MT_DEVICE,
	}, {
		.virtual	= IO_ADDRESS(MOXART_FLASH_BASE),
		.pfn		= __phys_to_pfn(MOXART_FLASH_BASE),
		.length		= SZ_16M,
		.type		= MT_DEVICE,
	}
};

void __init moxart_map_io(void)
{
	iotable_init(moxart_io_desc, ARRAY_SIZE(moxart_io_desc));
}
