/* MOXART GPIO (LEDs and RESET pin) driver (based on MOXA sources)
 * Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/io.h>
#include <linux/irq.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/miscdevice.h>
#include <linux/fcntl.h>
#include <linux/poll.h>
#include <linux/proc_fs.h>
#include <linux/spinlock.h>
#include <linux/rtc.h>
#include <linux/timer.h>
#include <linux/ioport.h>
#include <linux/kmod.h>

#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>

#include <mach/hardware.h>
#include <mach/irqs.h>
#include <mach/gpio.h>

#define MOXA_MISC_MINOR		105
#define RESET_POLL_TIME		(HZ/5)
#define RESET_TIMEOUT		(HZ * 5)
#define IOCTL_SW_READY_ON	1
#define IOCTL_SW_READY_OFF	2

static struct timer_list reset_timer;

static DEFINE_SPINLOCK(reset_lock);
static DEFINE_SPINLOCK(gpio_lock);

static unsigned long reset_time_end, reset_time_interval;
static int led_on_off_flag;
static struct work_struct reset_pin_detected_queue;

void moxart_gpio_mp_set(u32 gpio)
{
	unsigned long flags;

	spin_lock_irqsave(&gpio_lock, flags);
	writel(readl(IO_ADDRESS(MOXART_PMU_BASE)+0x100) |
		gpio, IO_ADDRESS(MOXART_PMU_BASE)+0x100);
	spin_unlock_irqrestore(&gpio_lock, flags);
}

void moxart_gpio_mp_clear(u32 gpio)
{
	unsigned long flags;

	spin_lock_irqsave(&gpio_lock, flags);
	writel(readl(IO_ADDRESS(MOXART_PMU_BASE)+0x100) &
		~gpio, IO_ADDRESS(MOXART_PMU_BASE)+0x100);
	spin_unlock_irqrestore(&gpio_lock, flags);
}

void moxart_gpio_inout(u32 gpio, bool inout)
{
	unsigned long flags;
	void __iomem *ioaddr = (void __force __iomem *)
		GPIO_PIN_DIRECTION(IO_ADDRESS(MOXART_GPIO_BASE));

	spin_lock_irqsave(&gpio_lock, flags);
	(inout) ? writel(readl(ioaddr) | gpio, ioaddr) :
			  writel(readl(ioaddr) & ~gpio, ioaddr);
	spin_unlock_irqrestore(&gpio_lock, flags);
}

void moxart_gpio_set(u32 gpio, bool highlow)
{
	unsigned long flags;
	void __iomem *ioaddr = (void __force __iomem *)
		GPIO_DATA_OUT(IO_ADDRESS(MOXART_GPIO_BASE));

	spin_lock_irqsave(&gpio_lock, flags);
	(highlow) ? writel(readl(ioaddr) | gpio, ioaddr) :
				writel(readl(ioaddr) & ~gpio, ioaddr);
	spin_unlock_irqrestore(&gpio_lock, flags);
}

u32 moxart_gpio_get(u32 gpio)
{
	unsigned long flags;
	u32	ret;

	spin_lock_irqsave(&gpio_lock, flags);
	ret = readl(GPIO_PIN_DIRECTION(IO_ADDRESS(MOXART_GPIO_BASE)));
	if (ret & gpio) {
		/* printk(KERN_INFO "MOXART GPIO: moxart_gpio_get: readl(%x)"
		"=%x\n", GPIO_DATA_OUT(IO_ADDRESS(MOXART_GPIO_BASE)),
		readl(GPIO_DATA_OUT(IO_ADDRESS(MOXART_GPIO_BASE))));*/
		ret = readl(GPIO_DATA_OUT(IO_ADDRESS(MOXART_GPIO_BASE))) & gpio;
	} else {
		ret = readl(GPIO_DATA_IN(IO_ADDRESS(MOXART_GPIO_BASE))) & gpio;
	}
	spin_unlock_irqrestore(&gpio_lock, flags);
	return ret;
}

void moxart_gpio_write_byte(u8 data)
{
	int i;
	for (i = 0; i < 8; i++, data >>= 1) {
		moxart_gpio_set(GPIO_RTC_SCLK, GPIO_EM1240_LOW);
		moxart_gpio_set(GPIO_RTC_DATA, ((data & 1) == 1));
		udelay(GPIO_RTC_DELAY_TIME);
		moxart_gpio_set(GPIO_RTC_SCLK, GPIO_EM1240_HIGH);
		udelay(GPIO_RTC_DELAY_TIME);
	}
}

u8 moxart_gpio_read_byte(void)
{
	int i;
	u8 data = 0;
	for (i = 0; i < 8; i++) {
		moxart_gpio_set(GPIO_RTC_SCLK, GPIO_EM1240_LOW);
		udelay(GPIO_RTC_DELAY_TIME);
		moxart_gpio_set(GPIO_RTC_SCLK, GPIO_EM1240_HIGH);
		if (moxart_gpio_get(GPIO_RTC_DATA))
			data |= (1 << i);
		udelay(GPIO_RTC_DELAY_TIME);
	}
	return data;
}

void reset_pin_detected(struct work_struct *work)
{
	/* char *argv[2], *envp[5]; */

	if (in_interrupt())
		return;
	pr_info("MOXART GPIO: reset_pin_detected\n");

	/* commenting below, not everyone is going to
	 * have /bin/setdef binary.. */

	/*if (!current->fs->root) return;
	argv[0] = "/bin/setdef";
	argv[1] = 0;
	envp[0] = "HOME=/";
	envp[1] = "PATH=/sbin:/bin:/usr/sbin:/usr/bin";
	envp[2] = 0;
	call_usermodehelper(argv[0], argv, envp, 0);
	*/
}

void reset_poll(unsigned long ingore)
{
	unsigned long flags;

	spin_lock_irqsave(&reset_lock, flags);
	del_timer(&reset_timer);

	/* printk(KERN_INFO "MOXART GPIO: reset_poll: moxart_gpio_get"
	 * "(SW_READY_GPIO) = %x\n", moxart_gpio_get(SW_READY_GPIO)); */
	moxart_gpio_get(SW_READY_GPIO);
	if (!moxart_gpio_get(SW_RESET_GPIO)) {
		if (reset_time_end == 0) {
			reset_time_end = jiffies + RESET_TIMEOUT;
			reset_time_interval = jiffies + HZ;
		} else if (time_after(jiffies, reset_time_end)) {
			moxart_gpio_set(SW_READY_GPIO, GPIO_EM1240_HIGH);
			schedule_work(&reset_pin_detected_queue);
			goto poll_exit;
		} else if (time_after(jiffies, reset_time_interval)) {
			if (led_on_off_flag) {
				led_on_off_flag = 0;
				moxart_gpio_set(SW_READY_GPIO,
					GPIO_EM1240_HIGH);
			} else {
				led_on_off_flag = 1;
				moxart_gpio_set(SW_READY_GPIO,
					GPIO_EM1240_LOW);
			}
			reset_time_interval = jiffies + HZ;
		}
	} else if (reset_time_end) {
		reset_time_end = 0;
		led_on_off_flag = 1;
		moxart_gpio_set(SW_READY_GPIO, GPIO_EM1240_LOW);
	}
	reset_timer.function = reset_poll;
	reset_timer.expires = jiffies + RESET_POLL_TIME;
	add_timer(&reset_timer);

poll_exit:
	spin_unlock_irqrestore(&reset_lock, flags);
}

static int gpio_ioctl(struct inode *inode, struct file *file,
	unsigned int cmd, unsigned long arg)
{
	switch (cmd) {
	case IOCTL_SW_READY_ON:
		moxart_gpio_set(SW_READY_GPIO, GPIO_EM1240_LOW);
		break;
	case IOCTL_SW_READY_OFF:
		moxart_gpio_set(SW_READY_GPIO, GPIO_EM1240_HIGH);
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int gpio_open(struct inode *inode, struct file *file)
{
	if (MINOR(inode->i_rdev) == MOXA_MISC_MINOR)
		return 0;
	return -ENODEV;
}

static int gpio_release(struct inode *inode, struct file *file)
{
	return 0;
}

static const struct file_operations gpio_fops = {
	.owner		= THIS_MODULE,
	.llseek		= no_llseek,
	.ioctl		= gpio_ioctl,
	.open		= gpio_open,
	.release	= gpio_release,
};

static struct miscdevice gpio_dev = {
	MOXA_MISC_MINOR,
	"moxart_gpio",
	&gpio_fops
};

static void __exit gpio_exit(void)
{
	unsigned long flags;

	spin_lock_irqsave(&reset_lock, flags);
	del_timer(&reset_timer);
	spin_unlock_irqrestore(&reset_lock, flags);
	misc_deregister(&gpio_dev);
}

static int __init gpio_init(void)
{
	unsigned long flags;

	pr_info("MOXART GPIO (LEDs and RESET pin) driver\n");
	if (misc_register(&gpio_dev)) {
		pr_info(": misc_register failed!\n");
		return -ENOMEM;
	}

	moxart_gpio_mp_set(SW_READY_GPIO|SW_RESET_GPIO);
	moxart_gpio_inout(SW_READY_GPIO, GPIO_EM1240_OUTPUT);
	moxart_gpio_inout(SW_RESET_GPIO, GPIO_EM1240_INPUT);
	moxart_gpio_set(SW_READY_GPIO, GPIO_EM1240_HIGH);

	INIT_WORK(&reset_pin_detected_queue, reset_pin_detected);
	spin_lock_init(&reset_lock);
	spin_lock_init(&gpio_lock);
	spin_lock_irqsave(&reset_lock, flags);
	reset_time_end = 0;
	led_on_off_flag = 1;
	init_timer(&reset_timer);
	reset_timer.function = reset_poll;
	reset_timer.expires = jiffies + RESET_POLL_TIME;
	add_timer(&reset_timer);
	spin_unlock_irqrestore(&reset_lock, flags);

	return 0;
}

module_init(gpio_init);
module_exit(gpio_exit);

MODULE_DESCRIPTION("MOXART GPIO (LEDs and RESET pin) driver");
MODULE_LICENSE("GPL");

