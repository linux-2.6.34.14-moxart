/* Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#ifndef __MACH_GPIO_H__
#define __MACH_GPIO_H__

#include <mach/irqs.h>
/* #include <asm-generic/gpio.h> */

#define GPIO_EM1240_HIGH	1
#define GPIO_EM1240_LOW		0
#define GPIO_EM1240_OUTPUT	1
#define GPIO_EM1240_INPUT	0

#define SW_READY_GPIO		(1<<27)
#define SW_RESET_GPIO		(1<<25)

#define GPIO_RTC_SCLK		(1<<5)  /* GPIO5 */
#define GPIO_RTC_DATA		(1<<6)  /* GPIO6 */
#define GPIO_RTC_RESET		(1<<7)  /* GPIO7 */
#define GPIO_RTC_MASK		(GPIO_RTC_SCLK | GPIO_RTC_DATA | GPIO_RTC_RESET)

#define GPIO_DATA_OUT(base_addr)			(base_addr + 0x00)
#define GPIO_DATA_IN(base_addr)				(base_addr + 0x04)
#define GPIO_PIN_DIRECTION(base_addr)		(base_addr + 0x08)
/*
#define GPIO_RTC_RESERVED(base_addr)			(base_addr + 0x0C)
#define GPIO_RTC_DATA_SET(base_addr)			(base_addr + 0x10)
#define GPIO_RTC_DATA_CLEAR(base_addr)			(base_addr + 0x14)
#define GPIO_RTC_PIN_PULL_ENABLE(base_addr)		(base_addr + 0x18)
#define GPIO_RTC_PIN_PULL_TYPE(base_addr)		(base_addr + 0x1C)
#define GPIO_RTC_INT_ENABLE(base_addr)			(base_addr + 0x20)
#define GPIO_RTC_INT_RAW_STATE(base_addr)		(base_addr + 0x24)
#define GPIO_RTC_INT_MASKED_STATE(base_addr)	(base_addr + 0x28)
#define GPIO_RTC_INT_MASK(base_addr)			(base_addr + 0x2C)
#define GPIO_RTC_INT_CLEAR(base_addr)			(base_addr + 0x30)
#define GPIO_RTC_INT_TRIGGER(base_addr)			(base_addr + 0x34)
#define GPIO_RTC_INT_BOTH(base_addr)			(base_addr + 0x38)
#define GPIO_RTC_INT_RISE_NEG(base_addr)		(base_addr + 0x3C)
#define GPIO_RTC_BOUNCE_ENABLE(base_addr)		(base_addr + 0x40)
#define GPIO_RTC_BOUNCE_PRE_SCALE(base_addr)	(base_addr + 0x44)
*/

#define GPIO_RTC_PROTECT_W		0x8E
#define GPIO_RTC_PROTECT_R		0x8F
#define GPIO_RTC_YEAR_W			0x8C
#define GPIO_RTC_YEAR_R			0x8D
#define GPIO_RTC_DAY_W			0x8A
#define GPIO_RTC_DAY_R			0x8B
#define GPIO_RTC_MONTH_W		0x88
#define GPIO_RTC_MONTH_R		0x89
#define GPIO_RTC_DATE_W			0x86
#define GPIO_RTC_DATE_R			0x87
#define GPIO_RTC_HOURS_W		0x84
#define GPIO_RTC_HOURS_R		0x85
#define GPIO_RTC_MINUTES_W		0x82
#define GPIO_RTC_MINUTES_R		0x83
#define GPIO_RTC_SECONDS_W		0x80
#define GPIO_RTC_SECONDS_R		0x81
#define GPIO_RTC_DELAY_TIME		8		/* 8 usecond */
#define GPIO_RTC_IS_OPEN        0x01	/* means /dev/rtc is in use */
#define GPIO_PIO(x)				(1 << x)

#ifndef __ASSEMBLY__

extern __init_or_module void moxart_gpio_mp_set(u32 gpio);
extern __init_or_module void moxart_gpio_mp_clear(u32 gpio);
extern __init_or_module void moxart_gpio_inout(u32 gpio, bool inout);
extern __init_or_module void moxart_gpio_set(u32 gpio, bool highlow);
extern __init_or_module u32 moxart_gpio_get(u32 gpio);
extern __init_or_module void moxart_gpio_write_byte(u8 data);
extern __init_or_module u8 moxart_gpio_read_byte(void);

#endif	/* __ASSEMBLY__ */


#endif /* __MACH_GPIO_H__ */
