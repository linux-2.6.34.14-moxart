/* Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * These are data structures found in platform_device.dev.platform_data,
 * and describing board-specific data needed by drivers.  For example,
 * which pin is used for a given GPIO role.
 *
 * In 2.6, drivers should strongly avoid board-specific knowledge so
 * that supporting new boards normally won't require driver patches.
 * Most board-specific knowledge should be in arch/.../board-*.c files.
 */

#ifndef __ASM_ARCH_BOARD_H
#define __ASM_ARCH_BOARD_H

extern void moxart_init_irq(void);
extern void moxart_map_io(void);
extern void moxart_register_uart(void);
extern void moxart_add_ethernet_devices(void);
extern void moxart_add_rtc_device(void);
extern void moxart_add_dma_device(void);
extern struct sys_timer moxart_timer;

struct moxart_eth_data {
	unsigned int base_addr;
	u8 mac_addr_flash_offset;
	u8 irq;
};


#endif
