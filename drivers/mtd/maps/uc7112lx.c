/* UC-7112-LX flash mapping driver
 * Copyright (C) 2013 Jonas Jensen <jonas.jensen@gmail.com>
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version. */

#include <mach/hardware.h>

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/io.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>


#define RW_PART0_OF	0x0

/* bootloader for MOXART CPU */
#define RW_PART0_SZ	0x40000

#define RW_PART1_OF	(RW_PART0_OF+RW_PART0_SZ)
#define RW_PART1_SZ	(0x200000-RW_PART0_SZ)	/* kernel */

#define RW_PART2_OF	(RW_PART1_OF+RW_PART1_SZ)
#define RW_PART2_SZ	0x800000				/* root fs */

#define RW_PART3_OF	(RW_PART2_OF+RW_PART2_SZ)
#define RW_PART3_SZ	0x600000				/* user fs */

#define CONFIG_FLASH_MEM_BASE 0x80000000
#define CONFIG_FLASH_SIZE 0x01000000

static struct map_info moxart_flash_map = {
	.name       = "UC-7112-LX-PLUS",
	.bankwidth  = 2,
	.phys       = CONFIG_FLASH_MEM_BASE,
	.size       = CONFIG_FLASH_SIZE,
	.virt       = (void __iomem *)IO_ADDRESS(CONFIG_FLASH_MEM_BASE),

/* MTD: Unable to init map for flash */
/*	.virt		= 0xf4000000,  */

};

static struct mtd_partition moxart_flash_partitions[] = {
	{
		.name = "Bootloader",
		.offset = RW_PART0_OF,
		.size = RW_PART0_SZ
	},
	{
		.name = "LinuxKernel",
		.offset = RW_PART1_OF,
		.size = RW_PART1_SZ,
/*		.mask_flags	=	MTD_WRITEABLE */
	},
	{
		.name = "RootFileSystem",
		.offset = RW_PART2_OF,
		.size = RW_PART2_SZ,
/*		.mask_flags = MTD_WRITEABLE */
	},
	{
		.name = "UserDisk",
		.offset = RW_PART3_OF,
		.size = RW_PART3_SZ
	}
};

/*struct map_info moxart_flash_map = {
	.name = "UC7110LX V2 FLASH",
	.size = CONFIG_FLASH_SIZE,
	.bankwidth = 2,
	.phys = CONFIG_FLASH_MEM_BASE,
	.virt = IO_ADDRESS(CONFIG_FLASH_MEM_BASE),
};

static int mtd_reboot(struct notifier_block *n, unsigned long code, void *p)
{
	if(code != SYS_RESTART)
				return NOTIFY_DONE;

	*( u16 *)(IO_ADDRESS(CONFIG_FLASH_MEM_BASE) + (0x55 * 2)) = 0xb0;
	*( u16 *)(IO_ADDRESS(CONFIG_FLASH_MEM_BASE) + (0x55 * 2)) = 0xff;
		return NOTIFY_DONE;
}

static struct notifier_block mtd_notifier = {
	notifier_call:  mtd_reboot,
	next:           NULL,
	priority:       0
};*/

static struct mtd_info *moxart_mtd;

int __init init_flash(void)
{
	pr_notice(
		"MTD: MOXART flash: %d-bit bus, mapping: 0x%x at 0x%x\n",
		moxart_flash_map.bankwidth*8,
		CONFIG_FLASH_SIZE, CONFIG_FLASH_MEM_BASE);
	simple_map_init(&moxart_flash_map);

	moxart_mtd = do_map_probe("cfi_probe", &moxart_flash_map);
	if (moxart_mtd) {
		moxart_mtd->owner = THIS_MODULE;
		pr_notice("MTD: Using static partition definition\n");
		add_mtd_partitions(moxart_mtd, moxart_flash_partitions,
			ARRAY_SIZE(moxart_flash_partitions));
	} else {
		pr_err("MTD: map probe failed for flash\n");
		iounmap(moxart_flash_map.virt);
		return -ENXIO;
	}

	/* return -ENXIO; */
	return 0;
}

int __init init_moxart_flash(void)
{
	int status;

	status = init_flash();
	if (status)
		pr_err("MTD: Unable to init map for flash\n");
	/*else
		register_reboot_notifier(&mtd_notifier); */
	return status;
}


static void __exit cleanup_moxart_flash(void)
{
	if (moxart_mtd) {
		/* unregister_reboot_notifier(&mtd_notifier); */
		del_mtd_partitions(moxart_mtd);
		map_destroy(moxart_mtd);
	}
}

module_init(init_moxart_flash);
module_exit(cleanup_moxart_flash);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jonas Jensen");
MODULE_DESCRIPTION("MTD map driver for UC-7112-LX");
